import { PostsModule } from '../posts/posts.module';
import { UserModule } from '../user/user.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommentEntity } from './models/comment.entity';
import { CommentsService } from './services/comments.service';
import { CommentsController } from './controller/comments.controller';

@Module({
  imports: [TypeOrmModule.forFeature([CommentEntity]), PostsModule, UserModule],
  controllers: [CommentsController],
  providers: [CommentsService]
})
export class CommentsModule {}
