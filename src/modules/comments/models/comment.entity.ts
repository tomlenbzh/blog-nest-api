import { PostEntity } from '@app/modules/posts/models/post.entity';
import { UserEntity } from '@app/modules/user/models/user.entity';
import { GenericEntity } from '@app/shared/generic.entity';
import { Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn, Column } from 'typeorm';

@Entity({ name: 'comments' })
export class CommentEntity extends GenericEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: '' })
  content: string;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.comments, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
    nullable: false
  })
  @JoinColumn({ name: 'userId' })
  user: UserEntity;

  @ManyToOne(() => PostEntity, (post: PostEntity) => post.comments, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
    nullable: false
  })
  @JoinColumn({ name: 'postId' })
  post: PostEntity;
}
