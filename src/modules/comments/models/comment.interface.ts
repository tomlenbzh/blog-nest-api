import { IPost } from '@app/modules/posts/models/post.interface';
import { IUser } from '@app/modules/user/models/user.interface';

export interface IComment {
  id?: number;
  user: IUser;
  post: IPost;
  content: string;
  createdAt?: Date;
  updatedAt?: Date;
}
