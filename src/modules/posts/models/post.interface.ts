import { IComment } from '@app/modules/comments/models/comment.interface';
import { IUser } from '@app/modules/user/models/user.interface';

export interface IPost {
  id?: number;
  title?: string;
  body?: string;
  createdAt?: Date;
  updatedAt?: Date;
  user?: IUser;
  likes?: IPost[];
  comments?: IComment[];
}
