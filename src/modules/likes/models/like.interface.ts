import { IPost } from '@app/modules/posts/models/post.interface';
import { IUser } from '@app/modules/user/models/user.interface';
import { LikeType } from './like.entity';

export interface ILike {
  id: number;
  type: LikeType;
  user: IUser;
  post: IPost;
}
