import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostsModule } from '../posts/posts.module';
import { UserModule } from '../user/user.module';
import { LikesController } from './controller/likes.controller';
import { LikeEntity } from './models/like.entity';
import { LikesService } from './services/likes.service';

@Module({
  imports: [TypeOrmModule.forFeature([LikeEntity]), PostsModule, UserModule],
  providers: [LikesService],
  controllers: [LikesController]
})
export class LikesModule {}
