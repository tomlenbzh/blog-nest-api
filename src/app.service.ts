import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getAppLanding(): string {
    return 'Welcome on our blog API!';
  }
}
